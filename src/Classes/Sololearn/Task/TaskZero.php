<?php
declare(strict_types=1);

namespace App\Sololearn\Task;

use App\Sololearn\AbstractTask;


/**
 * Class TaskZero
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Sololearn\Task
 */
class TaskZero extends AbstractTask
{
    /**
     * Do some setup on initialization.
     */
    public function setup(): void
    {
        // TODO: Implement setup() method.
    }

    /**
     * Runs the codeblock.
     */
    public function execute(): void
    {
        if ( !null ) {
            echo '1';
        }
        echo '0';
        if ( null ) {
            echo '0';
        }
    }
}