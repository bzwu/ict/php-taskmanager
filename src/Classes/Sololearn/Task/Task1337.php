<?php
declare(strict_types=1);

namespace App\Sololearn\Task;

use App\Sololearn\AbstractTask;

/**
 * Class Task1337
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Sololearn\Task
 */
class Task1337 extends AbstractTask
{
    /**
     * Do some setup on initialization.
     */
    public function setup(): void
    {

    }

    /**
     * Runs the codeblock.
     */
    public function execute(): void
    {
        for ( $i = 1; $i < 8; $i++ ) {
            if ( 5 % $i == 0 && $i != 1 ) {
                echo 8 & $i++;
            } else {
                echo $i++;
            }
        }
    }
}