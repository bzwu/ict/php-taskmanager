<?php
declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

$kernel = new \App\Core\Bootstrap();
$kernel->boot();

$taskManager = new \App\Sololearn\SoloLearnTaskManager( [
    'Zero',
    '1337',
    'For'
] );
$taskManager->run();