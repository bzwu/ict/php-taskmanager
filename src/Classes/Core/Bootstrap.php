<?php
declare(strict_types=1);

namespace App\Core;


/**
 * Class Bootstrap
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Core
 */
class Bootstrap
{
    public function boot()
    {

    }
}