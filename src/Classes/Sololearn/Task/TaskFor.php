<?php
declare(strict_types=1);

namespace App\Sololearn\Task;

use App\Sololearn\AbstractTask;


/**
 * Class TaskFor
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Sololearn\Task
 */
class TaskFor extends AbstractTask
{
    /**
     * Do some setup on initialization.
     */
    public function setup(): void
    {
        // TODO: Implement setup() method.
    }

    /**
     * Runs the codeblock.
     */
    public function execute(): void
    {
        for ( $i = 1; $i <= 10; $i++ ) {
            ${"a" . "$i"} = 11 - $i;
        }
        echo $a1 + $a10 - $a5;
    }
}