<?php
declare(strict_types=1);

namespace App\Sololearn;

/**
 * Class AbstractTask
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Sololearn
 */
abstract class AbstractTask implements Task
{
    public static function isRegistered( SoloLearnTaskManager $taskManager ): bool
    {
        return array_key_exists( static::class, $taskManager->getTasks() );
    }
}