<?php
declare(strict_types=1);

namespace App\Sololearn;

use App\Sololearn\Task\Task1337;
use App\Sololearn\Task\TaskFor;
use App\Sololearn\Task\TaskZero;

/**
 * Class SoloLearnTaskManager
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App
 */
class SoloLearnTaskManager
{
    /**
     * @var Task[]
     */
    private $tasks = [];

    /**
     * @var string[]
     */
    private $tasksToExecute = [];

    /**
     * @param array $tasksToRun
     */
    public function __construct( array $tasksToRun = [] )
    {
        $this->registerTasks( [
            new Task1337(),
            new TaskZero(),
            new TaskFor()
        ] );

        foreach ( $tasksToRun as $task ) {
            /** @var Task $taskFixture */
            $taskFixture = 'App\\Sololearn\\Task\\Task' . ucfirst( strtolower( $task ) );
            if ( class_exists( $taskFixture ) && $taskFixture::isRegistered( $this ) ) {
                $this->tasksToExecute[] = $taskFixture;
            }
        }
    }

    public function run()
    {
        $counter = 0;
        foreach ( $this->tasksToExecute as $className => $task ) {
            $this->tasks[$task]->execute();
            $counter++;
            if ( $counter < count( $this->tasksToExecute ) ) {
                echo '<hr>';
            }
        }
    }

    /**
     * @param Task[] $tasks
     */
    private function registerTasks( array $tasks = [] )
    {
        foreach ( $tasks as $task ) {
            $task->setup();
            $this->tasks[get_class( $task )] = $task;
        }
    }

    /**
     * @return Task[]
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }
}