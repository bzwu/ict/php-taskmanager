<?php
declare(strict_types=1);

namespace App\Sololearn;


/**
 * Class TaskInterface
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Sololearn
 */
interface Task
{
    /**
     * Do some setup on initialization.
     */
    public function setup(): void;

    /**
     * Runs the codeblock.
     */
    public function execute(): void;

    /**
     * @param SoloLearnTaskManager $taskManager
     *
     * @return bool
     */
    public static function isRegistered( SoloLearnTaskManager $taskManager ): bool;
}